<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblPacientes;
use App\Http\Controllers\dataControllers;

class PacientesController extends Controller
{
    public function search(Request $request)
    {
        $data = TblPacientes::where('documento','=',$request->documento)->first();
        if(is_null($data))
        {
            $curl = new dataControllers();
            try {
                $data=$curl->callAPI('http://citas.esepopayan.gov.co/api/paciente/get?key='.(md5('citas'.date('Ymd'))).'&documento='.$request->documento);
                $data = json_decode($data);
            } catch (\Throwable $th) {
                $data = null;
            }
        }
        return $this->sendResponse($data,!is_null($data));
    }
}
