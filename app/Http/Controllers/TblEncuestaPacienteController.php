<?php

namespace App\Http\Controllers;
use Session;
use App\Models\TblEncuestaPaciente;
use App\Models\TblPacientes;
use App\Models\TblEncuestaPacientesMedicamento;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class TblEncuestaPacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TblEncuestaPaciente::
        with('tbl_medicamentos')->
        with('tbl_modalidad_atencion')->
        with('tbl_motivo_consulta')->
        with('tbl_paciente.tbl_eps')->
        with('tbl_puntos_atencion')->
        with('tbl_tos_opcione')->
        with('tbl_fiebre_opcione')->
        with('user')->
        get();
        return $this->sendResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function paciente($paciente)
    {
        $paciente=(object)$paciente;
        $pacientes = TblPacientes::where('documento','=',$paciente->documento)->first();
        if(is_null($pacientes))
        {
            $pacientes = new TblPacientes();
        }
        $barrio=null;
        try {
            $barrio=is_null($paciente->barrio)?null:$paciente->barrio;
        } catch (\Throwable $th) {
            $barrio=null;
        }
        $pacientes->documento             = $paciente->documento;
        $pacientes->nombre_primero        = $paciente->nombre_primero;
        $pacientes->nombre_segundo        = $paciente->nombre_segundo;
        $pacientes->apellido_primero      = $paciente->apellido_primero;
        $pacientes->apellido_segundo      = $paciente->apellido_segundo;
        $pacientes->fecha_nacimiento      = $paciente->fecha_nacimiento;
        $pacientes->documento_tipo_id     = $paciente->documento_tipo_id;
        $pacientes->direccion             = $paciente->direccion;
        $pacientes->barrio                = $barrio;
        $pacientes->eps_id                = $paciente->eps_id;
        $pacientes->celular1              = $paciente->celular1;
        $pacientes->email                 = $paciente->email;
        $pacientes->direccion_observacion = $paciente->direccion_observacion;

        $pacientes->save();
        return $pacientes->id;

    }
    public function store(Request $request)
    {
        if(!is_null($request->motivo_consulta))
        {        
            $data = new TblEncuestaPaciente();
            $data->paciente_id                          = $this->paciente($request->paciente);
            $data->motivo_consulta_id                   = $request->motivo_consulta;
            $data->modalidad_atencion_id                = $request->modalidad_atencion_id;
            $data->observacion                          = $request->observacion;
            $data->si_tuvo_contacto_paciente_covid      = $request->si_tuvo_contacto_paciente_covid;
            $data->si_tuvo_contacto_diagnosticado_covid = $request->si_tuvo_contacto_diagnosticado_covid;
            $data->tos_opciones_id                      = $request->tos_opciones_id;
            $data->fiebre_opciones_id                   = $request->fiebre_opciones_id;
            $data->temperatura                          = $request->temperatura;
            $data->malestar_general                     = $request->malestar_general;
            $data->fatiga                               = $request->fatiga;
            $data->dolor_garganta                       = $request->dolor_garganta;
            $data->dificultad_respirar                  = $request->dificultad_respirar;
            $data->tomado_medicamentos_gripa            = $request->tomado_medicamentos_gripa;
            $data->user_id                              = Auth::user()->id;
            $data->punto_atencion_id                    = Session::get('id_punto_atencion');
            $data->save();
            foreach($request->tomado_medicamentos_gripaarray as $temp)
            {
                $medicamentos= new TblEncuestaPacientesMedicamento();
                $medicamentos->tbl_encuesta_pacientes_id    =$data->id;
                $medicamentos->tbl_medicamentos_id          =$temp['id'];
                $medicamentos->save();

            }
            return $this->sendResponse($data);
        }
    }
        
        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        $data = TblEncuestaPaciente::
        with('tbl_medicamentos')->
        with('tbl_modalidad_atencion')->
        with('tbl_paciente.tbl_eps')->
        with('tbl_motivo_consulta')->
        with('tbl_puntos_atencion')->
        with('tbl_tos_opcione')->
        with('tbl_fiebre_opcione')->
        
        with('user')->
        where('id','=',$id)->
        first();
        return $this->sendResponse($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
