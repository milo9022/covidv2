<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblEps;
use App\Models\TblDocumentoTipo;
use App\Models\TblPuntosAtencion;
use App\Models\TblMotivoConsulta;
use App\Models\TblModalidadAtencion;
use App\Models\TblFiebreOpcione;
use App\Models\TblTosOpcione;
use App\Models\TblMedicamento;
use App\Exports\encuestaExport;
use Maatwebsite\Excel\Facades\Excel;

class dataControllers extends Controller
{
    public function test()
    {
        return Excel::download(new encuestaExport, 'Encuesta.xlsx');
    }
    public function puntosAtencion()
    {
        $data = TblPuntosAtencion::orderBy('nombre')->get();
        return $this->sendResponse($data);
    }
    public function EPS()
    {
        $data = TblEps::orderBy('nombre')->get();
        return $this->sendResponse($data);
    }
    public function documentosTipo()
    {
        $data = TblDocumentoTipo::all();
        return $this->sendResponse($data);
    }
    public function motivoConsulta()
    {
        $data = TblMotivoConsulta::all();
        return $this->sendResponse($data);
    }
    public function modalidadAtencion()
    {
        $data = TblModalidadAtencion::all();
        return $this->sendResponse($data);
    }

    public function tosOpciones()
    {
        $data = TblTosOpcione::all();
        return $this->sendResponse($data);
    }
    public function fiebreOpciones()
    {
        $data = TblFiebreOpcione::all();
        return $this->sendResponse($data);
    }
    public function medicamentos()
    {
        $data = TblMedicamento::all();
        return $this->sendResponse($data);
    }
    public function callAPI($url, $data=false){
        $curl = curl_init();
        if ($data)
        {
            $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

}
