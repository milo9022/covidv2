<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblEncuestaPaciente
 * 
 * @property int $id
 * @property int $paciente_id
 * @property string $motivo_consulta_id
 * @property int $modalidad_atencion_id
 * @property string $observacion
 * @property int $si_tuvo_contacto_paciente_covid
 * @property int $si_tuvo_contacto_diagnosticado_covid
 * @property int $tos
 * @property int $fiebre
 * @property int $fiebre_opciones_id
 * @property int $tos_opciones_id
 * @property float $temperatura
 * @property int $malestar_general
 * @property int $fatiga
 * @property int $dolor_garganta
 * @property int $dificultad_respirar
 * @property int $tomado_medicamentos_gripa
 * @property int $user_id
 * @property int $punto_atencion_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property TblFiebreOpcione $tbl_fiebre_opcione
 * @property TblModalidadAtencion $tbl_modalidad_atencion
 * @property TblPaciente $tbl_paciente
 * @property TblPuntosAtencion $tbl_puntos_atencion
 * @property TblTosOpcione $tbl_tos_opcione
 * @property User $user
 *
 * @package App\Models
 */
class TblEncuestaPaciente extends Model
{
	protected $table = 'tbl_encuesta_pacientes';

	protected $casts = [
		'paciente_id' => 'int',
		'modalidad_atencion_id' => 'int',
		'si_tuvo_contacto_paciente_covid' => 'int',
		'si_tuvo_contacto_diagnosticado_covid' => 'int',
		'tos' => 'int',
		'fiebre' => 'int',
		'fiebre_opciones_id' => 'int',
		'tos_opciones_id' => 'int',
		'temperatura' => 'float',
		'malestar_general' => 'int',
		'fatiga' => 'int',
		'dolor_garganta' => 'int',
		'dificultad_respirar' => 'int',
		'tomado_medicamentos_gripa' => 'int',
		'user_id' => 'int',
		'motivo_consulta_id' => 'int',
		'punto_atencion_id' => 'int',
	];

	protected $fillable = [
		'paciente_id',
		'motivo_consulta_id',
		'modalidad_atencion_id',
		'observacion',
		'si_tuvo_contacto_paciente_covid',
		'si_tuvo_contacto_diagnosticado_covid',
		'tos',
		'fiebre',
		'fiebre_opciones_id',
		'tos_opciones_id',
		'temperatura',
		'malestar_general',
		'fatiga',
		'dolor_garganta',
		'dificultad_respirar',
		'tomado_medicamentos_gripa',
		'user_id',
		'punto_atencion_id'
	];

	public function tbl_fiebre_opcione()
	{
		return $this->belongsTo(TblFiebreOpcione::class, 'fiebre_opciones_id');
	}

	public function tbl_modalidad_atencion()
	{
		return $this->belongsTo(TblModalidadAtencion::class, 'modalidad_atencion_id');
	}

	public function tbl_paciente()
	{
		return $this->belongsTo(TblPacientes::class, 'paciente_id');
	}

	public function tbl_puntos_atencion()
	{
		return $this->belongsTo(TblPuntosAtencion::class, 'punto_atencion_id');
	}

	public function tbl_tos_opcione()
	{
		return $this->belongsTo(TblTosOpcione::class, 'tos_opciones_id');
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
	
	public function tbl_motivo_consulta()
	{
		return $this->belongsTo(TblMotivoConsulta::class,'motivo_consulta_id');
	}
	public function tbl_medicamentos()
	{
		return $this->belongsToMany('App\Models\TblMedicamento', 'tbl_encuesta_pacientes_medicamentos','tbl_encuesta_pacientes_id','tbl_medicamentos_id');
	}
}
