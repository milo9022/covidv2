<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblEncuestaPacientesMedicamento
 * 
 * @property int $id
 * @property int $tbl_encuesta_pacientes_id
 * @property int $tbl_medicamentos_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblEncuestaPacientesMedicamento extends Model
{
	protected $table = 'tbl_encuesta_pacientes_medicamentos';

	protected $casts = [
		'tbl_encuesta_pacientes_id' => 'int',
		'tbl_medicamentos_id' => 'int'
	];

	protected $fillable = [
		'tbl_encuesta_pacientes_id',
		'tbl_medicamentos_id'
	];
}
