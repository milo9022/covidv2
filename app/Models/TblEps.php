<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblEps extends Model
{
    protected $table = 'tbl_eps';

    protected $fillable = ['id','nombre'];
}
