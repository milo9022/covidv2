<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblFiebreOpcione
 * 
 * @property int $id
 * @property string $nombre
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblFiebreOpcione extends Model
{
	protected $table = 'tbl_fiebre_opciones';

	protected $fillable = [
		'nombre'
	];
}
