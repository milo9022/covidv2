<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblModalidadAtencion extends Model
{
    protected $table = 'tbl_modalidad_atencion';

    protected $fillable = ['id','nombre'];
}
