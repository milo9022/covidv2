<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblMotivoConsulta extends Model
{
    protected $table = 'tbl_motivo_consulta';

    protected $fillable = ['id','nombre'];
}
