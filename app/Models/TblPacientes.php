<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPacientes extends Model
{
    protected $table = 'tbl_pacientes';

    protected $fillable = ['id','nombre_primero','nombre_segundo','apellido_primero','apellido_segundo','documento_tipo_id','documento','direccion','barrio','eps_id','celular1','telefono','email'];

	public function tbl_eps()
	{
		return $this->belongsTo(TblEps::class, 'eps_id');
	}
}
