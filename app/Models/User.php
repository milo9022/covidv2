<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Aug 2019 18:50:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria

class User  extends Authenticatable
{
	use Notifiable;
	use SoftDeletes; //Implementamos 
	protected $casts = [
		'activo' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = ['id','nombre_primero','nombre_segundo','apellido_primero','apellido_segundo','documento','activo','email','created_at','updated_at','deleted_at','id_punto_atencion','id_area'];
    public function roles()
    {
        return $this
            ->belongsToMany('App\Role')
            ->withTimestamps();
    }
}
