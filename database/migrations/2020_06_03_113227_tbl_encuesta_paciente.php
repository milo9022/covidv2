<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblEncuestaPaciente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_encuesta_pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('paciente_id');
            $table->integer('motivo_consulta_id');
            $table->unsignedBigInteger('modalidad_atencion_id');
            $table->text('observacion');
            $table->integer('si_tuvo_contacto_paciente_covid')->default(0);
            $table->integer('si_tuvo_contacto_diagnosticado_covid')->default(0);
            $table->unsignedBigInteger('fiebre_opciones_id')->nullable();
            $table->unsignedBigInteger('tos_opciones_id')->nullable();
            $table->double('temperatura')->default(0);
            $table->integer('malestar_general')->default(0);
            $table->integer('fatiga')->default(0);
            $table->integer('dolor_garganta')->default(0);
            $table->integer('dificultad_respirar')->default(0);
            $table->integer('tomado_medicamentos_gripa')->default(0);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('punto_atencion_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_encuesta_pacientes');
    }
}
