<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPacientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_primero');
            $table->string('nombre_segundo')->nullable()->default(null);
            $table->string('apellido_primero');
            $table->string('apellido_segundo')->nullable()->default(null);
            $table->date('fecha_nacimiento')->nullable()->default(null);
            $table->integer('documento_tipo_id')->nullable()->default(null);
            $table->string('documento');
            $table->string('direccion')->nullable()->default(null);
            $table->string('direccion_observacion')->nullable()->default(null);
            $table->string('barrio')->nullable()->default(null);
            $table->integer('eps_id')->nullable()->default(null);
            $table->string('celular1')->nullable()->default(null);
            $table->string('telefono')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_pacientes');
    }
}
