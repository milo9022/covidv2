<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblEncuestaPacientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_encuesta_pacientes', function(Blueprint $table)
		{
			$table->foreign('modalidad_atencion_id', 'fk_id_modalidad_atencion_id_encuesta_pacientes')->references('id')->on('tbl_modalidad_atencion')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('paciente_id', 'fk_id_pacientes_id_encuesta_pacientes')->references('id')->on('tbl_pacientes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('punto_atencion_id', 'fk_id_puntos_atencion_id_encuesta_pacientes')->references('id')->on('tbl_puntos_atencion')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'fk_users_id_encuesta_pacientes')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_encuesta_pacientes', function(Blueprint $table)
		{
			$table->dropForeign('fk_id_modalidad_atencion_id_encuesta_pacientes');
			$table->dropForeign('fk_id_pacientes_id_encuesta_pacientes');
			$table->dropForeign('fk_id_puntos_atencion_id_encuesta_pacientes');
			$table->dropForeign('fk_users_id_encuesta_pacientes');
		});
	}

}
