<?php

use Illuminate\Database\Seeder;

class TblEpsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_eps')->delete();
        
        \DB::table('tbl_eps')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'ASMET',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'SALUD VIDA',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'EMSSANAR',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'AIC',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'MALLAMAS',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'SECRETARIA DE SALUD',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'OTROS',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'NUEVA EPS',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
        ));
        
        
    }
}