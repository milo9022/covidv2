<?php

use Illuminate\Database\Seeder;

class TblFiebreOpcionesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_fiebre_opciones')->delete();
        
        \DB::table('tbl_fiebre_opciones')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'SIN FIEBRE',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'FIEBRE MAYOR A 3 DIAS',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'FRIEBRE MENOR A 3 DIAS',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
        ));
        
        
    }
}