<?php

use Illuminate\Database\Seeder;

class TblMedicamentosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_medicamentos')->delete();
        
        \DB::table('tbl_medicamentos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'ACETAMINOFEN',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'DICLOFENALCO',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'ANTIHISTAMINICO',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'IBUPROFENO',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
        ));
        
        
    }
}