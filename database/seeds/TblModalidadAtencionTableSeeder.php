<?php

use Illuminate\Database\Seeder;

class TblModalidadAtencionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_modalidad_atencion')->delete();
        
        \DB::table('tbl_modalidad_atencion')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Tele consulta',
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Presencial',
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
        ));
        
        
    }
}