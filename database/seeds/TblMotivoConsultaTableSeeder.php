<?php

use Illuminate\Database\Seeder;

class TblMotivoConsultaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_motivo_consulta')->delete();
        
        \DB::table('tbl_motivo_consulta')->insert(array (
            array (
                'nombre' => 'Toma de exámenes de laboratorio',
                'created_at' => date('Y-m-d h:m:s'),
                'updated_at' => NULL,
            ),
            array (
                'nombre' => 'Solicitud de historia Clínica',
                'created_at' => date('Y-m-d h:m:s'),
                'updated_at' => NULL,
            ),
            array (
                'nombre' => 'vacunación',
                'created_at' => date('Y-m-d h:m:s'),
                'updated_at' => NULL,
            ),
            array (
                'nombre' => 'Urgencia Odontológica',
                'created_at' => date('Y-m-d h:m:s'),
                'updated_at' => NULL,
            ),
            array (
                'nombre' => 'Inicio planificacion familiar',
                'created_at' => date('Y-m-d h:m:s'),
                'updated_at' => NULL,
            ),
            array (
                'nombre' => 'Otros',
                'created_at' => date('Y-m-d h:m:s'),
                'updated_at' => NULL,
            )
        ));
        
        
    }
}