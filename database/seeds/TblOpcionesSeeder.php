<?php

use Illuminate\Database\Seeder;

class TblOpcionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_tos_opciones')->insert(array (
            array (
                'id' => 1,
                'nombre' => 'TOS SIN ESPUTO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            array (
                'id' => 2,
                'nombre' => 'TOS CON ESPUTO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
        ));

        \DB::table('tbl_fiebre_opcioness')->insert(array (
            array (
                'id' => 1,
                'nombre' => 'FIEBRE SIN TOS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            array (
                'id' => 2,
                'nombre' => 'FIEBRE CON TOS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
        ));

    }
}
