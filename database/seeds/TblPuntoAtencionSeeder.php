<?php

use Illuminate\Database\Seeder;

class TblPuntoAtencionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_puntos_atencion')->delete();
        
        \DB::table('tbl_puntos_atencion')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'CENTRO DE SALUD SUR OCCIDENTE',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'HOSPITAL MARIA OCCIDENTE',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'CENTRO DE SALUD LOMA DE LA VIRGEN',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'CENTRO DE SALUD SURORIENTE',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'CENTRO DE SALUD YANACONAS',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'CENTRO DE SALUD 31 MARZO',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'HOSPITAL DEL NORTE TORIBIO MAYA',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'CENTRO DE SALUD PUEBLILLO',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'CENTRO DE SALUD BELLO HORIZONTE',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'GRUPO EXTRAMURAL1',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'GRUPO EXTRAMURAL2',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'UNIDAD MOVIL ODONTOLOGICA',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => 'GRUPO EXTRAMURAL3',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ),
        ));
    }
}
