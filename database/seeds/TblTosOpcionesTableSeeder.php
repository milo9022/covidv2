<?php

use Illuminate\Database\Seeder;

class TblTosOpcionesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_tos_opciones')->delete();
        
        \DB::table('tbl_tos_opciones')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'SIN TOS',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'TOS CON ESPUTO',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'TOS SIN ESPUTO',
                'created_at' => date('Y-m-d H:m:i'),
                'updated_at' => date('Y-m-d H:m:i'),
            ),
        ));
        
        
    }
}